# CNFO - Containerized Network Function Orchestrator
Containerized Network Function Orchestrator provides mangement/orchestration template for the containerized networking applications.

## Description
CNFO, by leveraging the cloud-native and Kubernetes-cluster technologies, are mainly implemented, validated and deployed on Aarch64 architecture.
The CNFO architecture is shown in the following figure:
!["Containerized Network Function Orchestrator"](doc/imgs/CNF-KxS.png "Containerized Network Function Orchestrator")

## Getting started
...

## Examples
...

## Key features
* Template solution to plug-in any networking application
* Focus on features meant for networking applications including HA and Scaling
* Controller and worker on the same node or different nodes
* Support for edge to cloud deployment – K3S, Micro-K8S, K8S

## Use cases

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
With the exceptions recorded below, CNFO is distributed under the terms of the Apache License Version 2.0.
SPDX Identifier|TB Approval Date|GB Approval Date|File name
:--|:--:|:--:|--:
1.MIT|10/23/2019|02/10/2020|xxx/xxx/xxxxx.h
2.BSD-2-Clause|10/23/2019|12/18/2021|xxx/xxx/xxxxx.c

Some files in CNFO contain a different license statement. Those files are licensed under the license contained in the file itself.

CNFO also bundles patch files, which are applied to the sources of the various packages. Those patches are not covered by the license of CNFO. Instead, they are covered by the license of the software to which the patches are applied. When said software is available under multiple licenses, the CNFO patches are only provided under the publicly accessible licenses.

CNFO uses first line of the file to be SPDX tag. In case of *#!* scripts, SPDX tag can be placed in 2nd line of the file.

For example, to label a file as subject to the Apache-2.0 license, the following text would be used:
```
// SPDX-License-Identifier: Apache-2.0
```
or
```
#!/usr/bin/env bash
# SPDX-License-Identifier: Apache-2.0
```
